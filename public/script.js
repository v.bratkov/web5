/*jslint
    browser
*/
function calc() {
    const fI = document.getElementById("firstInput");
    const sI = document.getElementById("secondInput");

    if ((/^\d+(?:\.\d+)?$/).test(fI.value) && (/^\d+(?:\.\d+)?$/).test(sI.value)) {
        document.getElementById("result").innerHTML = "Результат:" +
        (fI.value * sI.value);
    } else {
        window.alert("Только целые или десятичные c точкой!");
    }
}

window.addEventListener("DOMContentLoaded", function () {
    const bt = document.getElementById("button1");

    bt.addEventListener("click", calc);
});
